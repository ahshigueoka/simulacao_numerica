classdef VanDerPol < handle
    properties (SetAccess=public)
        m;
        b;
        k;
        epsilon;
        gamma;
        omega;
        phi;
    end
    
    methods
        function obj = VanDerPol(m, b, k, eps, gamma, omega, phi)
            obj.m = m;
            obj.b = b;
            obj.k = k;
            obj.epsilon = eps;
            obj.gamma = gamma;
            obj.omega = omega;
            obj.phi = phi;
        end
        
        function dx = ss_equations(obj, t, x)
            dx = zeros(2, 1);
            
            dx(1) = x(2);
            dx(2) = -obj.epsilon/obj.m*(x(1)^2-obj.b)*x(2)-obj.k/obj.m*x(1);
        end
    end
    
end

