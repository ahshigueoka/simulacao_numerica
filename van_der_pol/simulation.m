clear all

% Parameters of the Van der Pol equation, where:
% m*d^2x/dt^2 + epsilon*(x^2 - b)*dx/dt + k*x = gamma*cos(omega*t + phi)
m = 1;
k = 1;
epsilon_vec = [1 1e-1 1e-2 1e-3 1e-4];
b = 1;
gamma = 0;
omega = 0;
phi = 0;

ode_opt = odeset('RelTol', 1e-4, 'Abstol', 1e-6);
t_span = 0:0.1:100;
x_0 = [1 0];

dir_figuras = 'figures/';

num_casos = size(epsilon_vec, 2);

for it = 1:num_casos
    epsilon = epsilon_vec(it);
    sys = VanDerPol(m, b, k, epsilon, gamma, omega, phi);
    sim = SS_simulation(@sys.ss_equations, t_span, x_0, ode_opt, @ode45);

    sim.simulate

    figure(1)
    plot(sim.x(:, 1), sim.x(:, 2));
    title('Plano de fase')
    xlabel('Posicao')
    ylabel('Velocidade')
    print('-dtiff','-r200', sprintf('%sCaso_%d__Plano_de_fase', dir_figuras, it))

    %Solucao obtida por m�ltiplas escalas ou pelo m�todo das m�dias
    x_pert = 2./sqrt(3*exp(-epsilon.*t_span) + 1).*cos(t_span);
    figure(2)
    plot(sim.t, sim.x(:, 1), '-b', sim.t, x_pert, '--g')
    title('Posicao')
    xlabel('tempo')
    ylabel('Posicao')
    print('-dtiff','-r200', sprintf('%sCaso_%d__Resposta_no_tempo_posicao', dir_figuras, it))
end