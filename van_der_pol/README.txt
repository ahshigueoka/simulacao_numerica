Run the "simulation.m" script in order to compare the
time response of the simulated system and the time
response calculated through multiple scales perturbation
theory for varios values of epsilon.