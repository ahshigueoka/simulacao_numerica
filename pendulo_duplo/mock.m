function [t, x] = mock(x_0, t_span, conf)
    t = t_span(1):conf.InitialStep:t_span(2);
    SIZE = size(t, 2);
    x = zeros(SIZE, 4);
    x(:, 1) = sin(t) + x_0(1);
    x(:, 2) = sin(t + pi/2)+ x_0(2);
    x(:, 3) = sin(t + pi)+ x_0(3);
    x(:, 4) = sin(t + 3/2*pi)+ x_0(4);
end