function Frame = animacao_pendulo_duplo(t, x, FRAME_RATE)
    % TODO Encapsular os par�metros geom�tricos do sistema.
    l_1 = .5;
    l_2 = .4;
    %g = 9.80665;
    %Omega = sqrt(2 * g / (l_1 + l_2));
    figure(1);
    numFrames = length(t);
    t_end = t(numFrames);

    % Taxa de atualiza��o desejada
    period = 1 / FRAME_RATE;
    t_movie = 0:period:t_end;
    movieFrames = length(t_movie);
    
    %Interpolar os resultados da simula��o
    x_movie = interp1(t, x, t_movie, 'spline');

    Frame(movieFrames) = struct('cdata',[],'colormap',[]);

    for i = 1:movieFrames
        % Plotar o tri�ngulo
        axis([-2.5 2.5 -2.5 2.5]);
        axis square
        fill([0 0.1 -0.1], [0 0.1 0.1], 'black')
        set(gca,'NextPlot','add');
        x_1 = l_1 * sin(x_movie(i, 1));
        y_1 = -l_1 * cos(x_movie(i, 1));
        x_2 = x_1 + l_2 * sin(x_movie(i, 3));
        y_2 = y_1 - l_2 * cos(x_movie(i, 3));
        % Plotar as barras
        plot([0 x_1 x_2], [0 y_1 y_2])
        radius = 0.1;
        radius2 = 0.05;
        % Plotar pino
        rectangle('Position',[-radius2/2, -radius2/2, radius2, radius2],...
        'Curvature',[1,1],...
        'FaceColor','k');
        % Plotar corpo 1
        rectangle('Position',[x_1 - radius/2, y_1 - radius/2, radius, radius],...
        'Curvature',[1,1],...
        'FaceColor','r');
        % Plotar corpo 2
        rectangle('Position',[x_2 - radius/2, y_2 - radius/2, radius, radius],...
        'Curvature',[1,1],...
        'FaceColor','r');
        Frame(i) = getframe;
        set(gca,'NextPlot','replaceChildren');
    end
end