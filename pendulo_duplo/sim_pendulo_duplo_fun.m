% SEM5914 - Simulação Numérica em Dinâmica Não-Linear
% Augusto Hirao Shigueoka
%
% Script - Simulação das equações do pêndulo duplo usando ode45
%
%=============================================================================
% Condições iniciais e configuração
%%----------------------------------------------------------------------------
function [t, x] = sim_pendulo_duplo_fun(x_0, t_span, conf)
    %x_0 = [0 30*pi 0 60*pi];
    %t_span = [0 10];
    %conf = odeset('RelTol', 1e-4, 'AbsTol', 1e-5, 'InitialStep', 1e-4, 'MaxStep', 1e-3);

    l_1 = .5; % m
    l_2 = .4; % m
    L = (l_1 + l_2) / 2; % m
    g = 9.80665; % m / s^2
    Omega = sqrt(g / L); % rad / s

    %=============================================================================
    % Adimensionalização das condições iniciais e do tempo
    %%----------------------------------------------------------------------------
    x_0_adim = [x_0(1) x_0(2)/Omega x_0(3) x_0(4)/Omega];
    t_span_adim = t_span * Omega;

    %=============================================================================
    % Simulação do sistema
    %%----------------------------------------------------------------------------
    [t, x] = ode45(@ss_pendulo_duplo, t_span_adim, x_0_adim, conf);
    
    % Converter para a forma dimensional
    t = t / Omega;
end