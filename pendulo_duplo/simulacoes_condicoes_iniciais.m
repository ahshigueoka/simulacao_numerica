%=============================================================================
% Simula��o dos casos com v�rias condi��es iniciais
%=============================================================================

close all
clear all
clc

%% ============================================================================
% Condi��es iniciais
%=============================================================================
% Perturba��o
delta = 0.01*pi;
x_0_array = [pi/20 0 pi/15 0;
             0 0 0 0;
              pi, 0,  pi, 0;
              pi, 0, -pi, 0;
             -pi, 0,  pi, 0;
             -pi, 0, -pi, 0;
             20/180*pi 0, -25/180*pi 0;
             120/180*pi 0 15/180*pi 0;
             120/180*pi 2*pi 130/180*pi 2*pi;
             0 30*pi 0 60*pi];
%% ===========================================================================
% Diret�rio das figuras
%=============================================================================
dir_figuras = 'figures/';
dir_resultados = 'results/';

%% ===========================================================================
% Tempo de simula��o
%=============================================================================
t_span = [0 10];

%% ===========================================================================
% Configura��es do ode45
%=============================================================================
ode_conf = odeset('RelTol', 1e-4, 'AbsTol', 1e-5, 'InitialStep', 1e-4, 'MaxStep', 1e-3);

%% ===========================================================================
% Simula��es e armazenamento dos resultados
%=============================================================================
% Para cada condi��o inicial, rodar uma simula��o, plotar os resultados e
% salvar as figuras
num_casos = size(x_0_array, 1);

for it = 1:1:num_casos
    % Simular o sistema com a i-�sima condi��o inicial
    [t, x] = sim_pendulo_duplo_fun(x_0_array(it, :), t_span, ode_conf);
    %[t, x] = mock(x_0_array(it, :), t_span, ode_conf);
    % Plotar o plano de fase para theta1
    figure(1)
    grid on
    plot(x(:, 1), x(:, 2))
    title('Plano de fase para \theta_{1}')
    xlabel('\theta_{1}')
    ylabel('\omega_{1}')
    print('-dtiff','-r200', sprintf('%sCaso_%d__Plano_de_fase_theta1', dir_figuras, it))
    
    % Plotar o plano de fase para o theta2
    figure(2)
    plot(x(:, 3), x(:, 4))
    title('Plano de fase para \theta_{2}')
    xlabel('\theta_{2}')
    ylabel('\omega_{2}')
    print('-dtiff','-r200', sprintf('%sCaso_%d__Plano_de_fase_theta2', dir_figuras, it))

    % Valores ao longo do tempo
    figure(3)
    % Theta 1
    subplot(4, 1, 1)
    plot(t, x(:, 1))
    xlabel('tempo')
    ylabel('\theta_{1}')
    % Omega 1
    subplot(4, 1, 2)
    plot(t, x(:, 2))
    xlabel('tempo')
    ylabel('\omega_{1}')
    % Theta 2
    subplot(4, 1, 3)
    plot(t, x(:, 3))
    xlabel('tempo')
    ylabel('\theta_{2}')
    % Omega 2
    subplot(4, 1, 4)
    plot(t, x(:, 4))
    xlabel('tempo')
    ylabel('\omega_{2}')
    print('-dtiff','-r200', sprintf('%sCaso_%d__Resposta_no_tempo', dir_figuras, it))
    
    save(sprintf('%sCaso_%d__x_t.mat', dir_resultados, it), 'x', 't')
    
    clear x t
end

clear all
close all