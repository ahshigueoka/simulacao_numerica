% SEM5914 - Simulação Numérica em Dinâmica Não-Linear
% Augusto Hirao Shigueoka
%
% Script - Simulação das equações do pêndulo duplo usando ode45
%
%=============================================================================
% Condições iniciais e configuração
%%----------------------------------------------------------------------------

clear all

x_0 = [0 30*pi 0 60*pi];
t_span = [0 10];
anim = false;
conf = odeset('RelTol', 1e-4, 'AbsTol', 1e-5, 'InitialStep', 1e-4, 'MaxStep', 1e-3);

l_1 = .5; % m
l_2 = .4; % m
L = (l_1 + l_2) / 2; % m
g = 9.80665; % m / s^2
Omega = sqrt(g / L); % rad / s

%=============================================================================
% Adimensionalização das condições iniciais e do tempo
%%----------------------------------------------------------------------------
x_0_adim = [x_0(1) x_0(2)/Omega x_0(3) x_0(4)/Omega];
t_span_adim = t_span * Omega;

%=============================================================================
% Simulação do sistema
%%----------------------------------------------------------------------------
[t, x] = ode45(@ss_pendulo_duplo, t_span_adim, x_0_adim, conf);

%=============================================================================
% Resultados
%%----------------------------------------------------------------------------

format long

% Plotar o plano de fase para theta1
figure(1)
grid on
plot(x(:, 1), x(:, 2))
title('Plano de fase para \theta_{1}')
xlabel('\theta_{1}')
ylabel('\omega_{1}')
% Plotar o plano de fase para o theta2
figure(2)
plot(x(:, 3), x(:, 4))
title('Plano de fase para \theta_{2}')
xlabel('\theta_{2}')
ylabel('\omega_{2}')

% Valores ao longo do tempo
figure(3)
% Theta 1
subplot(4, 1, 1)
plot(t, x(:, 1))
xlabel('tempo')
ylabel('\theta_{1}')
% Omega 1
subplot(4, 1, 2)
plot(t, x(:, 2))
xlabel('tempo')
ylabel('\omega_{1}')
% Theta 2
subplot(4, 1, 3)
plot(t, x(:, 3))
xlabel('tempo')
ylabel('\theta_{2}')
% Omega 2
subplot(4, 1, 4)
plot(t, x(:, 4))
xlabel('tempo')
ylabel('\omega_{2}')

if anim
    % Animacao do pendulo
    figure(4);
    numFrames = length(t);

    % Taxa de atualiza��o desejada
    VIDEO_FRAME_RATE = 30;
    FRAME_RATE = 1000;
    period = Omega / FRAME_RATE;
    movieFrames = floor(t_span_adim(2) / period);

    Frame(movieFrames) = struct('cdata',[],'colormap',[]);

    j = 0;
    for i = 1:numFrames
        if t(i) > j * period
            % Plotar o tri�ngulo
            axis([-2.5 2.5 -2.5 2.5]);
            axis square
            fill([0 0.1 -0.1], [0 0.1 0.1], 'black')
            set(gca,'NextPlot','add');
            x_1 = l_1 / L*sin(x(i, 1));
            y_1 = -l_1 / L*cos(x(i, 1));
            x_2 = x_1 + l_2 / L*sin(x(i, 3));
            y_2 = y_1 - l_2 / L*cos(x(i, 3));
            % Plotar as barras
            plot([0 x_1 x_2], [0 y_1 y_2])
            radius = 0.1;
            radius2 = 0.05;
            % Plotar pino
            rectangle('Position',[-radius2/2, -radius2/2, radius2, radius2],...
            'Curvature',[1,1],...
            'FaceColor','k');
            % Plotar corpo 1
            rectangle('Position',[x_1 - radius/2, y_1 - radius/2, radius, radius],...
            'Curvature',[1,1],...
            'FaceColor','r');
            % Plotar corpo 2
            rectangle('Position',[x_2 - radius/2, y_2 - radius/2, radius, radius],...
            'Curvature',[1,1],...
            'FaceColor','r');
            j = j + 1;
            Frame(j) = getframe;
            set(gca,'NextPlot','replaceChildren');
        end
    end

    figure(5)
    axis([-2.5 2.5 -2.5 2.5])
    axis square
    movie(Frame, 1, VIDEO_FRAME_RATE)
end

%clear all