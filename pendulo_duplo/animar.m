case_num = 8;
load(sprintf('results/Caso_%d__x_t.mat', case_num));

m = animacao_pendulo_duplo(t, x, 100);

figure(2);
axis([-2.5 2.5 -2.5 2.5]);
axis square
movie(m, 1, 30);