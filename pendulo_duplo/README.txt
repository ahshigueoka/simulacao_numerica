In order to simulate the results, run the script "simulacoes_condicoes_inciais.m".
It will simulate each initial condition and save the results in the folder
"results" and the graphics in the folder "figures".
If you want to see the animation for one of the initial conditions,
run the script "simulacoes_condicoes_iniciais.m" first,
open the script "animar.m", identify the case number using the variable "case_num" and run the script "animar.m"