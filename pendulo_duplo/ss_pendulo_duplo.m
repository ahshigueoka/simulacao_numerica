% SEM5914 - Simulação Numérica em Dinâmica Não-Linear
% Augusto Hirao Shigueoka
%
% Simulação do pêndulo duplo
%
% Função ss_pendulo_duplo(t, x)
%
% Retorna as derivadas das variáveis de estado das equações dinâmicas do
% pêndulo duplo.
%
% t é instante atual.
% x é um vetor com as variáveis de estado.
%     x(1) representa o ângulo theta1 da barra 1
%     x(2) representa a velocidade angular omega2 da barra 1
%     x(3) representa o ângulo theta2 da barra 2
%     x(4) representa a velocidade angular omega2 da barra 2
%

function x_prime = ss_pendulo_duplo(t, x)
%=============================================================================
% Definição de propriedades físicas
%%----------------------------------------------------------------------------
l_1 = .5; % m
l_2 = .4; % m
m_1 = .5; % kg
m_2 = .2; % kg
g = 9.80665; % m/s^2

M = (m_1 + m_2)/2;
L = (l_1 + l_2)/2;
Omega = sqrt(g/L);

A = 2 * (l_1 / L)^2 / Omega^2;
B = m_2 / M * l_1 * l_2 / L^2 / Omega^2;
C = m_2 / M * l_1 * l_2 / L / Omega^2;
D = 2 * g * l_1 / (L^2 * Omega^2);
E = m_2 / M * l_1 * l_2 / L^2 / Omega^2;
F = m_2 / M * (l_2 / L)^2 / Omega^2;
G = m_2 / M * g * l_2 / (L^2 * Omega^2);

%=============================================================================
% Definição do sistema de equações diferenciais
%%----------------------------------------------------------------------------

det_J = A * F - B * E * cos(x(1) - x(3))^2;

x_prime = [ x(2);...
			(-C * F * sin(x(1)-x(3)) * x(4)^2 - D * F * sin(x(1)) - B * C / 2 * sin(2*(x(1)-x(3))) * x(2)^2 + B * G * sin(x(3)) * cos(x(1) - x(3))) / det_J;...
			x(4);...
			(C * E / 2 * sin(2 * (x(1) - x(3))) * x(4)^2 + D * E * cos(x(1) - x(3)) * sin(x(1)) + A * C * sin(x(1) - x(3)) * x(2)^2 - A * G * sin(x(3))) / det_J
			];

