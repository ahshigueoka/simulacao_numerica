function rotmat = rotmatz(theta)
    rotmat = [cos(theta) -sin(theta); ...
              sin(theta) cos(theta)];
end

