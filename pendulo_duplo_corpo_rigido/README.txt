Run the script "simulation.m" in order to simulate the double pendulum
with several initial conditions. The graphics will be saved in the
directory "figuras".
The script "animation.m" will show the behavior of the double pendulum.