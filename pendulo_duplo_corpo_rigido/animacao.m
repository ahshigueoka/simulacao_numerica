clear all
clc

m1 = 1;
m2 = 1;
r1 = 1;
rg1 = sqrt(2)/2;
rg2 = sqrt(2)/2;
g = 9.80665;
beta1 = -pi/4;
beta2 = -pi/4;
J1o = 1;
J2g = 1;

% Dimens�es usadas para plotar as figuras
polygon = [0 1 1 0;...
           0 0 1 1];
color_body1 = [.7 .7 1];
color_body2 = [.5 0 .1];
radius_pin = 0.1;

FRAME_RATE = 30;

ode_opt = odeset('RelTol', 1e-6, 'Abstol', 1e-8);
t_span = [0 60];
x_0 = [-pi/2+atan(.5)+pi 0 -pi/2 0];

sys = Pendulo_duplo(m1, m2, r1, rg1, rg2, g, beta1, beta2, J1o, J2g);
Omega = sqrt(sys.Omega_2);
t_span_adim = t_span*Omega;
x_0_adim = x_0 * [1 0 0 0;
                  0 1/Omega 0 0;
                  0 0 1 0
                  0 0 0 1/Omega];

sim = SS_simulation(@sys.ss_equations, t_span_adim, x_0_adim, ode_opt, @ode45);

sim.simulate

t = sim.t / sqrt(sys.Omega_2);

figure(1);
numFrames = length(t);

% Taxa de atualiza��o desejada
period = 1 / FRAME_RATE;
movieFrames = floor((t_span(2)-t_span(1)) / period);

Frame(movieFrames) = struct('cdata',[],'colormap',[]);

j = 0;
for i = 1:numFrames
    if t(i) > j * period
        % Plotar o tri�ngulo
        axis([-2.5 2.5 -2.5 2.5]);
        axis square
        fill([0 0.1 -0.1], [0 0.1 0.1], 'black')
        set(gca,'NextPlot','add');
        %Rotacionar o corpo 1
        body_1 = rotmatz(sim.x(i, 1)+beta1)*polygon;
        % Plotar corpo 1
        fill(body_1(1, :), body_1(2, :), color_body1);
        % Plotar pino
        rectangle('Position',[-radius_pin/2, -radius_pin/2, radius_pin, radius_pin],...
        'Curvature',[1,1],...
        'FaceColor','k');
        %Transladar e rotacionar o corpo 2
        body_2 = rotmatz(sim.x(i, 3)+beta2)*polygon+rotmatz(sim.x(i, 1)+beta1)*repmat([r1; 0], 1, size(polygon, 2));
        % Plotar corpo 2
        fill(body_2(1, :), body_2(2, :), color_body2);
        % Plotar o pino
        r_pin = rotmatz(sim.x(i, 1)+beta1)*[r1; 0];
        rectangle('Position',[r_pin(1)-radius_pin/2, r_pin(2)-radius_pin/2, radius_pin, radius_pin],...
        'Curvature',[1,1],...
        'FaceColor',[1 1 1]);
        j = j + 1;
        Frame(j) = getframe;
        set(gca,'NextPlot','replaceChildren');
    end
end

movie(Frame, 1, FRAME_RATE);