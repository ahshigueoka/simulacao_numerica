classdef Pendulo_duplo < handle
    properties (SetAccess=private)
        m1;
        m2;
        r1;
        rg1;
        rg2;
        g;
        beta1;
        beta2;
        J1o;
        J2g;
        J;
        M;
        Rg;
        Omega_2
    end
    
    methods
        function obj = Pendulo_duplo(m1, m2, r1, rg1, rg2, g, beta1, beta2, J1o, J2g)
            obj.m1 = m1;
            obj.m2 = m2;
            obj.r1 = r1;
            obj.rg1 = rg1;
            obj.rg2 = rg2;
            obj.g = g;
            obj.beta1 = beta1;
            obj.beta2 = beta2;
            obj.J1o = J1o;
            obj.J2g = J2g;
            
            obj.J = (obj.J1o+obj.r1^2*obj.m2+obj.J2g+obj.rg2^2*obj.m2) / 2;
            obj.M = (obj.m1 + obj.m2) / 2;
            obj.Rg = (obj.rg1 + obj.rg2) / 2;
            obj.Omega_2 = obj.M*obj.Rg*obj.g/obj.J;
        end
        
        function dx = ss_equations(obj, t, x)
            
            A = (obj.J1o+obj.r1^2*obj.m2) / obj.J;
            B = obj.m2*obj.r1*obj.rg2 / obj.J;
            C = obj.m1*obj.rg1*obj.g/obj.J/obj.Omega_2;
            D = obj.m2*obj.g*obj.r1/obj.J/obj.Omega_2;
            E = (obj.J2g+obj.rg2^2*obj.m2) / obj.J;
            F = obj.m2*obj.rg2*obj.g/obj.J/obj.Omega_2;
            det = A*E-B^2*cos(x(1)-x(3)+obj.beta1)^2;
            
            dx = zeros(4, 1);
            
            dx(1) = x(2);
            dx(2) = (-B^2/2*sin(2*(x(1)-x(3)+obj.beta1)) ...
                -B*E*sin(x(1)-x(3)+obj.beta1)*x(4)^2 ...
                -C*E*cos(x(1))-D*E*cos(x(1)+obj.beta1)...
                +B*F*cos(x(3))*cos(x(1)-x(3)+obj.beta1) ...
                ) / det;
            dx(3) = x(4);
            dx(4) = (A*B*sin(x(1)-x(3)+obj.beta1)*x(2)^2 ...
                +B^2/2*sin(2*(x(1)-x(3)+obj.beta1))*x(4)^2 ...
                +B*C*cos(x(1))*cos(x(1)-x(3)+obj.beta1) ...
                +B*D*cos(x(1)+obj.beta1)*cos(x(1)-x(3)+obj.beta1) ...
                -A*F*cos(x(3)) ...
                ) / det;
        end
    end
    
end

