clear all
clc

m1 = 1;
m2 = 1;
r1 = 1;
rg1 = sqrt(2)/2;
rg2 = sqrt(2)/2;
g = 9.80665;
beta1 = -pi/4;
beta2 = 0;
J1o = 1;
J2g = 1;

ode_opt = odeset('RelTol', 1e-6, 'Abstol', 1e-8);
t_span = [0 60];
x_0 = [0 0 0 0;
       -pi/2+atan(.5) 0 -pi/2 0];
sys = Pendulo_duplo(m1, m2, r1, rg1, rg2, g, beta1, beta2, J1o, J2g);
Omega = sqrt(sys.Omega_2);
t_span_adim = t_span*Omega;
x_0_adim = x_0*[1 0 0 0
                0 1/Omega 0 0
                0 0 1 0
                0 0 0 1/Omega];

dir_figuras = 'figures/';

num_casos = size(x_0, 1);

for it = 1:num_casos
    sim = SS_simulation(@sys.ss_equations, ...
        t_span_adim, ...
        x_0_adim(it, :), ...
        ode_opt, @ode45);

    sim.simulate

    figure(1)
    plot(sim.x(:, 1), sim.x(:, 2));
    title('Plano de fase do corpo 1')
    xlabel('\theta_{1}')
    ylabel('\omega_{1}')
    print('-dtiff','-r200', sprintf('%sCaso_%d__Plano_de_fase_corpo1', dir_figuras, it))
    
    figure(2)
    plot(sim.x(:, 3), sim.x(:, 4));
    title('Plano de fase do corpo 2')
    xlabel('\theta_{2}')
    ylabel('\omega_{2}')
    print('-dtiff','-r200', sprintf('%sCaso_%d__Plano_de_fase_corpo2', dir_figuras, it))
    
    figure(3)
    plot(sim.t, sim.x(:, 1));
    title('Resposta no tempo do corpo 1')
    xlabel('Tempo')
    ylabel('\theta_{1}')
    print('-dtiff','-r200', sprintf('%sCaso_%d__Resposta_tempo_corpo1', dir_figuras, it))
    
    plot(sim.t, sim.x(:, 3));
    title('Resposta no tempo do corpo 2')
    xlabel('Tempo')
    ylabel('\theta_{2}')
    print('-dtiff','-r200', sprintf('%sCaso_%d__Resposta_tempo_corpo2', dir_figuras, it))
end