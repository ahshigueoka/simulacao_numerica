clear all

t_span = [0, 100];
x_0 = [0, 0;...
       0.1, 0;...
       1e-10, 0;...
       1e-20, 0;...
       1, 0;...
       0, 1;...
       1, 1];
ode_opt = odeset('RelTol', 1e-4, 'RelTol', 1e-5);
dir_figuras = 'figures/';

for it=1:size(x_0, 1)
    [t, x] = ode45(@ss_equations, t_span, x_0(it, :), ode_opt);

    figure(1);
    plot(t, x(:, 1));
    title('Posicao');
    xlabel('t');
    ylabel('\theta');
    print('-dtiff','-r200', sprintf('%sCaso_%d__Resposta_no_tempo_angulo', dir_figuras, it))

    figure(2);
    plot(t, x(:, 2));
    title('Velocidade');
    xlabel('t');
    ylabel('\omega');
    print('-dtiff','-r200', sprintf('%sCaso_%d__Resposta_no_tempo_velocidade', dir_figuras, it))

    figure(3);
    plot(x(:, 1), x(:, 2));
    title('Plano de fase');
    xlabel('\theta');
    ylabel('\omega');
    print('-dtiff','-r200', sprintf('%sCaso_%d__Plano_de_fase', dir_figuras, it))
end