function dx = ss_equations(t, x)
    a = 1;
    g = 9.80665;
    h = 2;
    m = 1;
    c = 10;

    dx = zeros(2, 1);
    
    dx(1) = x(2);
    dx(2) = sin(x(1))/a*(-g + c*h/m/(a^2 + h^2 - 2*a*h*cos(x(1)))^1.5);
end

