function f = magnet_force(a, c, h, g, m, theta)
    f = sin(theta)/a*(-g + c*h/m/(a^2 + h^2 - 2*a*h*cos(theta))^1.5);
end