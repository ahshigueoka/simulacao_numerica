The script "plano_fase.m" will simulate the magnetic systems for several
initial conditions and save the graphics in the directory "figures".

"diagrama_estabilidade.m" will plot the stability diagram of the system
by varying the parameter "c".