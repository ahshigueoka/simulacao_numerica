clear all

c_span = [-4*pi 4*pi];
c_divs = 100;
theta_span = [-2*pi 2*pi];
theta_divs = 100;

area_limits = [-10 0];

a = 1*ones(theta_divs, c_divs);
c = linspace(c_span(1), c_span(2), c_divs);
g = 9.80665*ones(theta_divs, c_divs);
h = 2*ones(theta_divs, c_divs);
m = 1*ones(theta_divs, c_divs);
theta = linspace(theta_span(1), theta_span(2), theta_divs);

[C_vec, T_vec] = meshgrid(c, theta);

force = arrayfun(@magnet_force, a, C_vec, h, g, m, T_vec);

figure(1)
[CM, H] = contourf(C_vec, T_vec, force, [-100, 0]);
clabel(CM, H);
colorbar;
xlabel('c');
ylabel('theta');