%=============================================================================
%% Equação diferencial a ser calculada
fh = @(t, y) 1 + y^2;

% Condição inicial
y0 = 0.0;

% Intervalo de tempo
time_limits = [0.0 1.4];

%=============================================================================
%% Runge-Kutta 4
% Configuração do método de Runge-Kutta 4
h_4 = 0.1;

% Solução calculada pelo método Runge-Kutta 4
[y_4, t_4] = rk4(fh, time_limits, y0, h_4);

y_e = tan(t_4);

% Cálculo do erro
erro_4 = y_4 - y_e;

%=============================================================================
%% Runge-Kutta 5
% Configuração do método de Runge-Kutta 5
h_5 = 0.1;

% Solução calculada pelo método Runge-Kutta 5
[y_5, t_5] = rk5(fh, time_limits, y0, h_5);

% Cálculo do erro
erro_5 = y_5 - y_e;

%=============================================================================
%% Runge-Kutta-Fehlberg 45

% Configuração do método de Runge-Kutta-Fehlberg 45
h_limits = [1e-6 0.2];
e_limits = [1e-8 1e-5];

% Solução calculada pelo método Runge-Kutta-Fehlberg 45
[y_45, t_45] = rkf45(fh, time_limits, y0, h_limits, e_limits);

% Solução exata
y_e45 = tan(t_45);

% Calcular o erro
erro_45 = y_45 - y_e45;

%=============================================================================
%% Apresenta��o dos resultados

resultados1 = [t_4; y_4; erro_4; y_5; erro_5]';
resultados2 = [t_45; y_45; erro_45]';
format long
% Mostrar os resultados
fprintf('%20s%20s%20s%20s%20s\n', 'Tempo', 'RK4', 'Erro', 'RK5', 'Erro RK5')
disp(resultados1)

% 45
disp('Runge-Kutta-Fehlberg 45')
fprintf('%20s%20s%20s\n', 'Tempo', 'RKF45', 'Erro RKF45')
disp(resultados2)
