% Runge-Kutta-Fehlberg de ordens 4 e 5
%
%-- função: rkf45(FUNCAO, [T0, T], Y0, [h_min, h_max], [e_min, e_max])
%
% Aplica o método de Runge-Kutta-Fehlberg a uma equação diferencial da forma
% dy/dt = f(t, y).
%
% FUNCAO é um handler para a função f(t, y).
%
% [T0, T] é o intervalo fechado dentro do qual os valores de tempo serão
% calculados para se aplicar o método de Runge-Kutta-Fehlberg.
%
% Y0 é o valor para a condição inicial.
%
% [h_min h_max] definem os limites para o passo de tempo utilizados para
% realizar os incrementos de t dentro do intervalo [T0, T].
% 
% [e_min e_max] definem os limites aceitáveis para o desvio obtido entre os
% valores de y(i) calculados através dos métodos de Runge-Kutta ordem 4 e
% ordem 5. Caso o erro e = |(y_4(i) - y_5(i))/h(i)| seja menor do que e_min,
% o passo de tempo h é aumentado. Caso e seja maior do que e_max, o passo de
% tempo h é reduzido. Caso e_min <= e <= e_max, o valor atual de h é mantido.
% Os novos valores de h nunca ultrapassarão os limites especificados, no
% entanto. Caso o novo valor de h seja menor do que h_min, h será mantido
% igual a h_min. Caso o novo valor de h seja maior do que h_max, h será
% mantido igual a h_max. Um aviso será emitido caso os valores limite de h
% sejam atingidos.
%
% Esta função retorna os resultados y(i) e t(i) para cada iteração do método
% em um vetor duplo.

function [y, t] = rkf45_2(fh, time_limits, Y0, h_limits, e_limits)
    % Vetor com os intantes de tempo a serem calculados
    % Alocando espaço o suficiente para o caso em que o número de passos seja
    % o máximo, com o menor passo possível.
    NumPassos = cast((time_limits(2)-time_limits(1))/h_limits(1) + 1, 'int64');
    t = zeros(1, NumPassos);
    % Vetor com os valores a serem calculados
    y = zeros(1, NumPassos);
    % Valores iniciais
    y(1) = Y0;
    t(1) = time_limits(1);
    % Iteração atual
    i = 1;
    % Passo da iteração. Começar com o maior possível para reduzido custo
    % computacional.
    h = h_limits(2);
    
    % Iterações do método
    while t(i) < time_limits(2)
        K1 = h * fh(t(i), y(i));
        K2 = h * fh(t(i) + 1/4 * h, y(i) + 1/4 * K1);
        K3 = h * fh(t(i) + 3/8 * h, y(i) + 3/32 * K1 + 9/32 * K2);
        K4 = h * fh(t(i) + 12/13 * h, y(i) + 1932/2197 * K1 - 7200/2197 * K2 + 7296/2197 * K3);
        K5 = h * fh(t(i) + h, y(i) + 439/216 * K1 - 8 * K2 + 3680/513 * K3 - 845/4104 * K4);
        K6 = h * fh(t(i) + 1/2 * h, y(i) - 8/27 * K1 + 2 * K2 - 3544/2565 * K3 + 1859/4104 * K4 - 11/40 * K5);
        % Estimar o erro da iteração.
        R = abs(1/360 * K1 - 128/4275 * K3 - 2197/75240 * K4 + 1/50 * K5 + 2/55 * K6)/h;

        if R <= e_limits(2)
            % Erro dentro do intervalo aceitável
            y(i+1) = y(i) + 25/216 * K1 + 1408/2565 * K3 + 2197/4104 * K4 - 1/5 * K5;
            t(i+1) = t(i) + h;
            % Passar para a próxima iteração
            i = i + 1;
        end

        % Alterar o valor de h
        s = 0.84 * nthroot(e_limits(2) / R, 4);
        if s < 0.1
            s = 0.1;
        elseif s > 10
            s = 10;
        end

        h = s * h;

        if h > h_limits(2)
            warning('Aviso: o limite superior de h foi atingido.')
            disp(i)
            h = h_limits(2);
        elseif h < h_limits(1)
            error('Erro: o limite inferior de h foi atingido. Recomenda-se utilizar um limite inferior de h menor ou uma tolerância maior.')
            %h = h_limits(1);
        end

        if h + t(i) > time_limits(2)
            h = time_limits(2) - t(i);
        end
    end

    % Retornar apenas a parte com os resultados
    y = y(1, 1:i);
    t = t(1, 1:i);

    return
end

