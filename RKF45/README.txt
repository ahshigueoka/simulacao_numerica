Execute "teste.m" in order to test all the functions and compare the results.

Warning: these programs were written in order to understand the procedures
that lie within the studied numerical methods. You shouldn't use these
functions in numerical simulations, since they are not optimized.