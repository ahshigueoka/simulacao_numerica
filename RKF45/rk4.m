% Runge-Kutta 4
%
%-- função: rk4(FUNCAO, [T0, T], Y0, h)
%
% Aplica o método de Runge-Kutta a uma equação diferencial da forma
% dy/dt = f(t, y).
%
% FUNCAO é um handler para a função f(t, y).
% [T0, T] é o intervalo fechado dentro do qual os valores de tempo serão
% calculados para se aplicar o método de Runge-Kutta.
% Y0 é o valor para as condição inicial.
% h é o passo de tempo para realizar os incrementos de t dentro do intervalo
% [T0, T]
%
% Esta função retorna os resultados y(i) e t(i) para cada iteração do método
% em uma vetor duplo.

function [y, t] = rk4(fh, time_limits, Y0, h)
    %Vetor com os intantes de tempo a serem calculados
    t = time_limits(1):h:time_limits(2);
    %Vetor com os valores a serem calculados
    y = zeros(1, length(t));
    %Valor inicial
    y(1) = Y0;
    
    % Iterações do método
    for i=1:(length(t)-1)
        K1 = h * fh(t(i), y(i));
        K2 = h * fh(t(i) + 1/4 * h, y(i) + 1/4 * K1);
        K3 = h * fh(t(i) + 3/8 * h, y(i) + 3/32 * K1 + 9/32 * K2);
        K4 = h * fh(t(i) + 12/13 * h, y(i) + 1932/2197 * K1 - 7200/2197 * K2 + 7296/2197 * K3);
        K5 = h * fh(t(i) + h, y(i) + 439/216 * K1 - 8 * K2 + 3680/513 * K3 - 845/4104 * K4);
        y(i+1) = y(i) + 25/216 * K1 + 1408/2565 * K3 + 2197/4104 * K4 - 1/5 * K5;
    end

    return
end

