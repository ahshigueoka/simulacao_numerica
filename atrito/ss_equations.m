function dx = ss_equations(t, x)
    dx = zeros(2, 1);
    dx(1) = x(2);
    dx(2) = -x(1) + F(x(2));
end