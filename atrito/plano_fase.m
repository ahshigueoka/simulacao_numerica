clear all

ode_opt = odeset('RelTol', 1e-4, 'AbsTol', 1e-5);
t_span = [0 100];
x_0 = [0 -5; 0 5; 5 5; 1.01 0; 1 0];
dir_figuras = 'figures/';

for it=1:size(x_0, 1)
    [t, x] = ode45(@ss_equations, t_span, x_0(it, :), ode_opt);
    
    figure(1)
    xlabel('x');
    ylabel('v');
    plot(x(:,1), x(:,2));
    print('-dtiff','-r200', sprintf('%sCaso_%d__Plano_de_fase', dir_figuras, it))
end