function out = F(dx)
    k = 0.5;
    v0 = 1;
    tol = 1e-3;
    
    if dx - v0 > tol
        out = -k*dx-1;
    elseif dx - v0 < -tol
        out = k*dx+1;
    else
        out = 0;
    end
end
        