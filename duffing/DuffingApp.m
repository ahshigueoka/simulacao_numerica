classdef DuffingApp < handle
    properties (SetAccess = public)
        MENU_OPTIONS;
        sys;
        factory;
        running_flag;
    end
    
%     % M�todos p�blicos
%     methods (Access = public)
%         % Construtor padr�o
%         function obj = DuffingApp()
%             obj.running_flag = true;
%             obj.factory = Factory();
%             obj.MENU_OPTIONS = obj.factory.create_operation_vector('menu_prompt.txt');
%         end
%         
%         % Fun��o principal
%         function retcode = OnExecute(obj)
%             obj.OnInit()
%             
%             while(obj.running_flag)
%                 opt = obj.prompt_user();
%                 
%                 disp(opt);
%                 obj.running_flag = false;
%                 obj.MENU_OPTIONS(opt).operation(obj.sys, obj);
%             end
%             
%             retcode = 0;
%         end
%     end
%     
%     % Fun��es utilit�rias
%     methods (Access = private)
%         % Inicializar os par�metros do sistema
%         function OnInit(obj)
%             disp('Inicializando...');
%         end
%         
%         % Print menu and prompts the user
%         function opt = prompt_user(obj)
%             num_options = length(obj.MENU_OPTIONS);
%             for op = 1:num_options
%                 fprintf('\t%d. %s\n', obj.MENU_OPTIONS(op).code, obj.MENU_OPTIONS(op).description);
%             end
%             opt = input('Entre com a sua op��o: ');
%         end
%     end
end

