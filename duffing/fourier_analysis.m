function [flag, app] = fourier_analysis(app)
    % Definir os instantes em que as amostras ser�o coletadas.
    t_sample = app.solver.t_span(1):app.solver.Ts:app.solver.t_span(2);
    % Interpolar atrav�s dos resultados para conseguir os valores
    % correspondentes.
    x_sample = interp1(app.t, app.x(:, 1), t_sample, 'cubic');
    
    
    num_samples = length(t_sample);
    
    freq_com = abs(fft(x_sample) / num_samples);
    
    freq_vec = 2*pi*app.sys.Omega*app.solver.Fs*linspace(0, 1, num_samples);
    
    figure(1)
    plot(freq_vec, freq_com);
    title('FFT');
    xlabel('frequencia');
    ylabel('magnitude');
    
    flag = true;
end

