clear all
close all
clc

% Condi��es iniciais e configura��o para cada caso.
caso.description = 'Oscilador de Duffing periodico, sem perturba��o';
caso.ID = 1;
caso.m = 1;
caso.c = 0.3;
caso.k = -1;
caso.epsilon = 1;
caso.gamma = 0;
caso.omega = 1;
caso.phi = 0;
caso.Fs = 10;
caso.Ts = 1 / caso(1).Fs;
caso.frame_rate = 30;
caso.t_span = [0, 100];
caso.x_0 = [1 1];
caso.rel_tol = 1e-3;
caso.abs_tol = 1e-10;
caso.settling_tol = 1e-3;
caso.settling_interval = 10;
caso.method = 'ode45';
caso.grid_range = [-2 2 -2 2];
caso.grid_step = [0.5, 0.5];
caso.dir_figuras = 'figures/';

% Condi��es iniciais e configura��o para cada caso.
caso(2).description = 'Oscilador de Duffing caotico';
caso(2).ID = 2;
caso(2).m = 1;
caso(2).c = 0.2;
caso(2).k = -1;
caso(2).epsilon = 1;
caso(2).gamma = 0.3;
caso(2).omega = 1;
caso(2).phi = 0;
caso(2).Fs = 10;
caso(2).Ts = 1 / caso(2).Fs;
caso(2).frame_rate = 30;
caso(2).t_span = [0, 100];
caso(2).x_0 = [1 1];
caso(2).rel_tol = 1e-3;
caso(2).abs_tol = 1e-10;
caso(2).settling_tol = 1e-3;
caso(2).settling_interval = 10;
caso(2).method = 'ode45';
caso(2).grid_range = [-2 2 -2 2];
caso(2).grid_step = [0.1, 0.1];
caso(2).dir_figuras = 'figures/';

% Condi��es iniciais e configura��o para cada caso.
caso(3).description = 'Oscilador de Duffing periodico linear, sem perturba��o';
caso(3).ID = 1;
caso(3).m = 1;
caso(3).c = 0.3;
caso(3).k = 1;
caso(3).epsilon = 0;
caso(3).gamma = 0;
caso(3).omega = 1;
caso(3).phi = 0;
caso(3).Fs = 10;
caso(3).Ts = 1 / caso(1).Fs;
caso(3).frame_rate = 30;
caso(3).t_span = [0, 50];
caso(3).x_0 = [1 0];
caso(3).rel_tol = 1e-3;
caso(3).abs_tol = 1e-10;
caso(3).settling_tol = 1e-3;
caso(3).settling_interval = 10;
caso(3).method = 'ode45';
caso(3).grid_range = [-4 4 -4 4];
caso(3).grid_step = [0.5, 0.5];
caso(3).dir_figuras = 'figures/';

% Condi��es iniciais e configura��o para cada caso.
caso(4).description = 'Oscilador de Duffing periodico linear, com perturba��o';
caso(4).ID = 1;
caso(4).m = 1;
caso(4).c = 0.3;
caso(4).k = 1;
caso(4).epsilon = 0;
caso(4).gamma = 1;
caso(4).omega = 1;
caso(4).phi = 0;
caso(4).Fs = 10;
caso(4).Ts = 1 / caso(1).Fs;
caso(4).frame_rate = 30;
caso(4).t_span = [0, 50];
caso(4).x_0 = [0 0];
caso(4).rel_tol = 1e-3;
caso(4).abs_tol = 1e-10;
caso(4).settling_tol = 1e-3;
caso(4).settling_interval = 10;
caso(4).method = 'ode45';
caso(4).grid_range = [-10 10 -10 10];
caso(4).grid_step = [0.5, 0.5];
caso(4).dir_figuras = 'figuras/';

% Condi��es iniciais e configura��o para cada caso.
caso(5).description = 'Oscilador de Duffing periodico n�o linear, bacia fractal';
caso(5).ID = 1;
caso(5).m = 1;
caso(5).c = 0.3;
caso(5).k = -0.5;
caso(5).epsilon = 0.5;
caso(5).gamma = 0.4;
caso(5).omega = 1;
caso(5).phi = 0;
caso(5).Fs = 10;
caso(5).Ts = 1 / caso(1).Fs;
caso(5).frame_rate = 30;
caso(5).t_span = [0, 100];
caso(5).x_0 = [0 0];
caso(5).rel_tol = 1e-3;
caso(5).abs_tol = 1e-10;
caso(5).settling_tol = 0.2;
caso(5).settling_interval = 10;
caso(5).method = 'ode45';
caso(5).grid_range = [-4 4 -4 4];
caso(5).grid_step = [0.01, 0.01];
caso(5).dir_figuras = 'figuras/';

caso(6).description = 'Oscilador harmonico';
caso(6).ID = 1;
caso(6).m = 1;
caso(6).c = 0.3;
caso(6).k = 1;
caso(6).epsilon = 0;
caso(6).gamma = 1;
caso(6).omega = 1;
caso(6).phi = 0;
caso(6).Fs = 10;
caso(6).Ts = 1 / caso(1).Fs;
caso(6).frame_rate = 30;
caso(6).t_span = [0, 100];
caso(6).x_0 = [0 0];
caso(6).rel_tol = 1e-4;
caso(6).abs_tol = 1e-10;
caso(6).settling_tol = 0.01;
caso(6).settling_interval = 10;
caso(6).method = 'ode45';
caso(6).grid_range = [-4 4 -4 4];
caso(6).grid_step = [0.5, 0.5];
caso(6).dir_figuras = 'figuras/';

num_casos = length(caso);

batch_mode = false;

if batch_mode
    for i = 1:num_casos
        if main('menu_prompt.txt', caso(i), batch_mode) ~= 0
            fprintf('Erro em main()');
            break
        end
    end
else
    % Perguntar qual caso simular
    case_id = input('Simular caso n�[n <= 0 para sair]: ');
    ok_flag = true;
    
    if case_id <= 0
        flag = false;
    end
    if case_id > num_casos
        fprintf('Esse caso n�o existe.\n');
    end
    if ok_flag
        if main('menu_prompt.txt', caso(case_id), batch_mode) ~= 0
            fprintf('Erro em main()');
        end
    end
end

