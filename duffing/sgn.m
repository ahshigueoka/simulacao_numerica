function y = sgn(x, tol)
%SGN Summary of this function goes here
%   Detailed explanation goes here
    if x + tol < 0
        y = -1;
    elseif x - tol > 0
        y = 1;
    else
        y = 0;
    end
end

