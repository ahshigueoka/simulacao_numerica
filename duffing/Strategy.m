classdef Strategy
    %STRATEGY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Abstract)
        ID;
        code;
        description;
    end
    
    methods (Abstract)
        operation(obj, sys, app)
    end
    
end

