function [flag, app] = phase_plane(app)

    figure(1)
    plot(app.x(:, 1), app.x(:, 2));
    title('Plano de fase')
    xlabel('Posicao')
    ylabel('Velocidade')
    
    flag = true;
end