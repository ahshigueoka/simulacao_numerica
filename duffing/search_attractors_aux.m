function [app] = search_attractors_aux(app, period)
    % Criar uma matriz com todos as condi��es iniciais
    points = repmat(app.grid.points, 1, 1);
    
    % Prealocar uma matriz com as trajet�rias de cada um dos pontos.
    t = app.solver.t_span(1):period:app.solver.t_span(2);
    x_len = length(t);
    clearvars t
    trajectories = zeros(length(points), 2, x_len);
    
    % Simular cada condi��o inicial, obtendo os pontos em cada frame
    num_points = length(points);
    for point_it = 1:num_points
        % Simular para a i-�sima condi��o inicial
        app.solver.x_0 = points(point_it, :);
        [app.t, app.x] = simulate(app.sys, app.solver);
        % Obter os pontos do Diagrama de Poincar�
        [~, x_poincare] = sample_aux(app, period);
        % Armazenar os resultados para este ponto.
        trajectories(point_it, :, :) = x_poincare';
        %Plotar 
        figure(1)
        plot(x_poincare(:, 1), x_poincare(:, 2), ':', ...
            'MarkerFaceColor', [0 0 1], ...
            'MarkerSize', 3);
        axis(app.grid.range)
    end
    
    num_frames = size(trajectories, 3);
    movie(num_frames) = struct('cdata',[],'colormap',[]);
    figure(1)
    % Para cada frame
    for f_i = 1:num_frames
        % Plotar os pontos
        plot(trajectories(:, 1, f_i), trajectories(:, 2, f_i), '.', ...
            'MarkerFaceColor', [1 0 0], ...
            'MarkerSize', 1);
        axis(app.grid.range)
        title('Plano de fase');
        xlabel('Posicao');
        ylabel('Velocidade');
        % Gravar a imagem e salvar como um frame da anima��o
        movie(f_i) = getframe();
    end
    
    % Salvar as anima��es
    app.movie = movie;
end

