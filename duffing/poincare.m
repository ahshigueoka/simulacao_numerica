function [flag, app] = poincare(app)
    period = app.sys.Omega*2*pi/app.sys.omega;
    
    [~, x_sample] = sample_aux(app, period);
    
    % Plotar no plano de fase os pontos para formar o diagrama
    % de Poincar�
    figure(1)
    plot(x_sample(:, 1), x_sample(:, 2), '.r', ...
        'MarkerFaceColor', [1 0 0], ...
        'MarkerSize', 1);
    title('Diagrama de Poincare');
    xlabel('Posicao');
    ylabel('Velocidade');
    
    flag = true;
end

