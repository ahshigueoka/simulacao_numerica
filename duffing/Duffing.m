classdef Duffing < handle
    properties (SetAccess=public)
        Omega;
        c_a;
        epsilon_a;
        gamma_a;
        omega_a;
        phi;
        k;
    end
    
    methods
        function obj = Duffing(m, c, k, eps, gamma, omega, phi)
            obj.Omega = sqrt(abs(k)/m);
            obj.k = k;
            obj.c_a = c / m / obj.Omega;
            obj.epsilon_a = eps / m / obj.Omega^2;
            obj.gamma_a = gamma / m / obj.Omega^2;
            obj.omega_a = omega / obj.Omega;
            obj.phi = phi;
        end
        
        function dx = ss_equations(obj, t, x)
            tol = 1e-12;
            % Esta fun��o utiliza as equa��es din�micas adimensionalizadas.
            dx = zeros(2, 1);
            
            dx(1) = x(2);
            dx(2) = -sgn(obj.k, tol)*x(1)-obj.epsilon_a*x(1)^3-obj.c_a*x(2)+obj.gamma_a*cos(obj.omega_a*t+obj.phi);
        end
    end
    
end

