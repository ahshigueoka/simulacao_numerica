% Runge-Kutta ordem 5, um passo
%
%-- função: rk5(FUNCAO, [T0, T], Y0, h)
%
% Aplica um passo do m�todo de Runge-Kutta a uma equa��o diferencial
% da forma
% dy/dt = f(t, y).
%
% FUNCAO: um handler para a fun��o f(t, y) que descreve o sistema em
% espa�o de estados.
% [T0, T]: o intervalo fechado dentro do qual os valores de tempo serão
% calculados para se aplicar o m�todo de Runge-Kutta.
% Y0 é o valor para as condi��o inicial.
% h: o passo de tempo para realizar os incrementos de t dentro do intervalo
% [T0, T]
%
% Esta fun��o retorna os resultados y(i) e t(i) para cada itera��oo do
% m�todo em um vetor duplo.

function [y, t] = rk5_step(fh, t, y0, h)
    % Uma itera��o do m�todo
    K1 = h * fh(t, y0);
    K2 = h * fh(t + 1/4 * h, y0 + 1/4 * K1);
    K3 = h * fh(t + 3/8 * h, y0 + 3/32 * K1 + 9/32 * K2);
    K4 = h * fh(t + 12/13 * h, y0 + 1932/2197 * K1 - 7200/2197 * K2 + 7296/2197 * K3);
    K5 = h * fh(t + h, y0 + 439/216 * K1 - 8 * K2 + 3680/513 * K3 - 845/4104 * K4);
    K6 = h * fh(t + 1/2 * h, y0 - 8/27 * K1 + 2 * K2 - 3544/2565 * K3 + 1859/4104 * K4 - 11/40 * K5);
    y = y0 + 16/135 * K1 + 6656/12825 * K3 + 28561/56430 * K4 - 9/50 * K5 + 2/55 * K6;
end

