function [flag, color] = get_attractor_color(attractors_list, point, tol)
    % Confere se o ponto representado por point converge para
    % algum dos atratores em attractors_list, dentro da toler�ncia
    % tol, usando a norma 2 para comparar. Retorna a cor branca
    % caso n�o tenha convergido para nenhum dos atratores.

    num_attractors = length(attractors_list);
    flag = false;
    
    for i = 1:num_attractors
        if norm(attractors_list(i).point - point, 2) < tol
            flag = true;
            color = attractors_list(i).color;
            return
        end
    end
    
    color = [1 1 1];
end

