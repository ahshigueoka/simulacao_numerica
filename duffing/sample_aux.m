function [t_sample, x_sample] = sample_aux(app, period)
    % Interpola atrav�s dos resultados a cada per�odo
    % period de tempo.
    
    % Criar um vetor com os instantes a serem amostrados.
    t_sample = app.solver.t_span(1):period:app.solver.t_span(2);
    % Interpolar atrav�s dos resultados para encontrar o valor
    % em cada m�ltiplo de um per�odo.
    x_sample = interp1(app.t, app.x, t_sample, 'cubic');
end

