function duff = Duffing_init(caso)
    duff.description = caso.description;
    duff.Omega = sqrt(abs(caso.k)/caso.m);
    duff.k = caso.k;
    duff.c_a = caso.c / caso.m / duff.Omega;
    duff.epsilon_a = caso.epsilon / caso.m / duff.Omega^2;
    duff.gamma_a = caso.gamma / caso.m / duff.Omega^2;
    duff.omega = caso.omega;
    duff.omega_a = caso.omega / duff.Omega;
    duff.phi = caso.phi;
    duff.tol = caso.abs_tol;
end

