function solver = Solver_init(caso)
    Omega = sqrt(abs(caso.k)/caso.m);
    solver.t_span = caso.t_span / Omega;
    solver.x_0 = caso.x_0 *[1,       0;
                            0, 1/Omega];
    solver.ode_opt = odeset('RelTol', caso.rel_tol, ...
                            'AbsTol', caso.abs_tol);
    solver.method = str2func(caso.method);
    solver.Fs = caso.Fs;
    solver.Ts = caso.Ts;
    solver.settling_tol = caso.settling_tol;
    solver.settling_interval = caso.settling_interval;
end

