Run the file "multicases.m". Choose one of the cases and
simulate.

This program was designed to be as much modular as possible.
It may be possible to use the functions used in this program
without actually running the main program, provided that you
give the right parameters.