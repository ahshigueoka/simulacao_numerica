function [flag, app] = search_attractors(app)
    period = app.sys.Omega*2*pi/app.sys.omega;
    app = search_attractors_aux(app, period);
    flag = true;
end

