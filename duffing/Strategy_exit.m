classdef Strategy_exit < Strategy
    properties
        ID;
        code;
        description;
    end
    
    methods (Access = public)
        function obj = Strategy_exit(op)
            obj.ID = op.ID;
            obj.code = op.code;
            obj.description = op.description;
        end
        
        % Implementation of abstract method
        function operation(obj, sys, app)
            app.running_flag = false;
            fprintf('Concluido\n');
        end
    end
    
end

