function dx = ss_equations(t, x, duff)
    % Esta fun��o utiliza as equa��es din�micas adimensionalizadas.
    dx = zeros(2, 1);

    dx(1) = x(2);
    dx(2) = -sgn(duff.k, duff.tol)*x(1)-duff.epsilon_a*x(1)^3-duff.c_a*x(2)+duff.gamma_a*cos(duff.omega_a*t+duff.phi);
end

