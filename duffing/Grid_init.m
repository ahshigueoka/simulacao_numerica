function grid = Grid_init(caso)
    % Esta fun��o passa os par�metros que ser�o usados
    % para construir a grade com os pontos usados para
    % encontrar a bacia de atra��o.
    %
    % Formato da grade:
    % 
    % Vetor de pontos, seguindo a seguinte ordem:
    %
    % 41 42 43 44 45 46 47 48 49 50
    % 31 32 33 34 35 36 37 38 39 40
    % 21 22 23 24 25 26 27 28 29 30
    % 11 12 13 14 15 16 17 18 19 20
    %  1  2  3  4  5  6  7  8  9 10
    %
    
    % Vetor no sentido de x
    x = caso.grid_range(1):caso.grid_step(1):caso.grid_range(2);
    y = caso.grid_range(1):caso.grid_step(2):caso.grid_range(2);
    
    % Construir a grade
    x_len = length(x);
    y_len = length(y);
    % Prealocar a matriz
    points = zeros(x_len*y_len, 2);
    
    % Inserir valores da grade de pontos
    for i = 1:x_len
        for j = 1:y_len
            points(i+(j-1)*x_len, :) = [x(i), y(j)];
        end
    end
    
    grid.points = points;
    grid.range = caso.grid_range;
    grid.step = caso.grid_step;
end

