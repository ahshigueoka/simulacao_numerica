clear all

m = 1;
c = 0.3;
k = 1;
epsilon = 0;
gamma = 0.5;
omega = 1.2;
phi = 0;

ode_opt = odeset('RelTol', 1e-6, 'Abstol', 1e-9);
Fs = 10;
Ts = 1/Fs;
t_span = 0:Ts:1000;
x_0 = [-1, 0.5];

dir_figuras = 'figuras/';

sys = Duffing(m, c, k, epsilon, gamma, omega, phi);
t_span_a = sys.Omega * t_span;
sim = SS_simulation(@sys.ss_equations, t_span, x_0, ode_opt, @ode45);

sim.simulate

num_samples = length(sim.t);
freq_com = abs(fft(sim.x) / num_samples);

figure(1)
plot(sim.t / sys.Omega, sim.x(:, 1));
title('Resposta no tempo');
xlabel('t');
ylabel('x');
print('-dtiff','-r200', sprintf('%sduff_fft__Resposta_no_tempo', dir_figuras));

figure(2)
plot(sim.x(:, 1), sim.x(:, 2));
title('Plano de fase');
xlabel('posi��o');
ylabel('velocidade');
print('-dtiff','-r200', sprintf('%sduff_fft__Plano_de_fase', dir_figuras));

figure(3)
freq_vec = Fs*linspace(0, 1, num_samples);
plot(freq_vec, freq_com);
title('FFT');
xlabel('frequencia');
ylabel('magnitude');
print('-dtiff','-r200', sprintf('%sduff_fft__FFT', dir_figuras));