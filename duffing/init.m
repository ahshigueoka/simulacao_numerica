function operations = init(filename)
    % Abrir o documento com as entradas de menu
    fid = fopen(filename, 'r', 'b', 'UTF-8');

    % Ler a primeira linha
    promptline = fgetl(fid);
    operations = [];
    % A primeira linha cont�m os cabe�alhos
    fields = parse_menu_prompt(promptline);
    num_fields = length(fields);
    
    % As linhas seguintes cont�m os valores
    promptline = fgetl(fid);
    % Continuar lendo at� terminar com o arquivo.
    while ischar(promptline)
        % Parsear as entradas desta linha
        values = parse_menu_prompt(promptline);
        % Organizar os campos com os seus valores.
        args = {fields{1}};
        args = cat(1, args, values{1});
        for i = 2:num_fields
            args = cat(1, args, fields{i});
            args = cat(1, args, values{i});
        end
        % Criar um registro para esta opera��o
        op_reg = struct(args{:});
        operations = cat(1, operations, op_reg);
        % Continuar lendo at� terminar com o arquivo.
        promptline = fgetl(fid);
    end

    fclose(fid);
end

% Parsear as entradas de uma linha
function fields = parse_menu_prompt(line)
    DELIM = ',';

    [token, remain] = strtok(line, DELIM);
    token = strtrim(token);
    fields = {token};

    while(not(isempty(remain)))
        [token, remain] = strtok(remain, DELIM);
        token = strtrim(token);
        fields = cat(1, fields, token);
    end
end