function [flag, app] = time_response(app)

    [app.t, app.x] = simulate(app.sys, app.solver);
    
    figure(1)
    plot(app.t, app.x(:, 1));
    title('Resposta no tempo')
    xlabel('Tempo')
    ylabel('Posicao')
    
    figure(2)
    plot(app.t, app.x(:, 2));
    title('Resposta no tempo')
    xlabel('Tempo')
    ylabel('Velocidade')
    
    flag = true;
end

