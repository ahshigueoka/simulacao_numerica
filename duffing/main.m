function ret = main(filename, caso, batch_mode)
% This is the function that should be run in order to execute the program.
% All the other functionalities are called from within this main function.
    running_flag = true;
    
    sys = Duffing_init(caso);
    solver = Solver_init(caso);
    grid = Grid_init(caso);

    app.sys = sys;
    app.solver = solver;
    app.grid = grid;
    app.abs_tol = caso.abs_tol;
    app.frame_rate = caso.frame_rate;
    app.t = 0;
    app.x = 0;
    app.movie = struct('cdata',[],'colormap',[]);
    app.batch_mode = batch_mode;
    app.dir_figuras = caso.dir_figuras;
    app.ID = caso.ID;

    operations = init(filename);
    while(running_flag)
        %clc
        fprintf('%s\n', app.sys.description);
        [running_flag, app]= program_loop(operations, app);
    end
    clean_up();
    
    ret = 0;
end

