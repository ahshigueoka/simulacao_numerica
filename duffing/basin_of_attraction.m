function [flag, app] = basin_of_attraction(app)
    
    if app.batch_mode
        % Se estiver em batch_mode, ler os par�metros fornecidos
        % TODO
    else
        correct_attractors = 'n';
        while(correct_attractors == 'n')
            % Sen�o, informar os atratores manualmente
            attractors = prompt_attractors();
            for i = 1:length(attractors)
                fprintf('Atrator[%d]:\n', i);
                fprintf('Coordenadas:\n   (%f, %f)\n', ...
                    attractors(i).point(1), ...
                    attractors(i).point(2));
                fprintf('Cor (RGB):\n (%f, %f, %f)\n', ...
                    attractors(i).color(1), ...
                    attractors(i).color(2), ...
                    attractors(i).color(3));
            end
            correct_attractors = input('Confirmar dados(s/n): ');
            while ~(correct_attractors == 'n' || ...
                    correct_attractors == 'N' || ...
                    correct_attractors == 's' || ...
                    correct_attractors == 'S')
                correct_attractors = input('Confirmar dados(s/n): ');
            end
        end
    end 
    
    
    % Criar um vetor contendo cada atrator e os pontos de sua
    % bacia de atra��o
    num_attractors = length(attractors);
    basins(num_attractors) = struct('attractor', [], 'points', []);
    for i = 1:num_attractors
        basins(i).attractor = attractors(i).point;
    end
    % Um vetor para manter as m�tricas dos atratores
    basins_num_points = zeros(1, num_attractors+1);
    
    % Para cada ponto do grid, simular e conferir onde o �ltimo
    % ponto se encontra. Se o ponto for estiver suficientemente
    % pr�ximo a um atrator, adicionar o ponto � bacia do atrator.
    num_points = length(app.grid.points);
    period = app.sys.Omega*2*pi/app.sys.omega;
    for point_i = 1:num_points
        % Simular para a i-�sima condi��o inicial
        app.solver.x_0 = app.grid.points(point_i, :);
        [app.t, app.x] = simulate(app.sys, app.solver);
        % Pegar os valores do diagrama de Poincar�
        [t_p, x_p] = sample_aux(app, period);
        % Comparar o valor de regime permanente com todos os
        % atratores.
        num_samples = length(t_p);
        
        % Pegar a cor correspondente se o ponto concidiu com
        % um dos atratores
        attractor_ind = get_attractor_ind(attractors, ...
            x_p(num_samples, :), ...
            app.solver.settling_tol);
        
        % Se coincidiu com um dos atratores, adicionar o ponto
        % � bacia do atrator correspondente.
        if attractor_ind ~= -1
            basins(attractor_ind).points = ...
                cat(1, basins(attractor_ind).points, ...
                app.grid.points(point_i, :));
            basins_num_points(attractor_ind) = basins_num_points(attractor_ind) + 1;
        else
            basins_num_points(num_attractors+1) = basins_num_points(num_attractors+1) + 1;
        end
        
        % Mostrar m�tricas da simula��o
        clc
        fprintf('[%3.2f%%] %d de %d condi��es iniciais simuladas.\n', point_i/num_points*100, point_i, num_points);
        for i = 1:num_attractors
            fprintf('Atrator[%d]: [%3.2f%%] %d pontos na bacia\n', ...
                i, ...
                basins_num_points(i) / num_points * 100, ...
                basins_num_points(i));
        end
        fprintf('[%3.2f%%] %d n�o pertencem a nenhuma bacia\n\n', ...
                basins_num_points(num_attractors+1) / num_points * 100, ...
                basins_num_points(num_attractors+1));
    end
    
    % Plotar a bacia de atra��o
    figure(1)
    clf(1, 'reset');
    set(gca,'NextPlot','add');
    
    for i = 1:num_attractors
        if size(basins(i).points, 1) > 0
            plot(basins(i).points(:, 1), ...
                basins(i).points(:, 2), ...
                '.', 'Color', attractors(i).color, 'MarkerSize', 1);
        end
    end
    
    axis(app.grid.range)
    axis square
    title('Bacia de atracao');
    xlabel('Posicao');
    ylabel('Velocidade');
    set(gca,'NextPlot','replaceChildren');
    
    % Salvar o gr�fico
    print('-dtiff','-r200', sprintf('%sCaso_%d__Bacia_de_atracao', app.dir_figuras, app.ID))
    
    flag = true;
end

