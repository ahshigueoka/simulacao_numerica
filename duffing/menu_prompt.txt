ID, function_name, description
1, setup, Configurar os parametros
2, time_response, Resposta no tempo
3, phase_plane, Plano de fase. Pre-requisito: opcao 2
4, phase_plane_grid, Plano de fase para a grade de condicoes iniciais
5, fourier_analysis, FFT da resposta no tempo. Pre-requisito: opcao 2
6, poincare, Diagrama de Poincare
7, search_attractors, Procurar atratores no diagrama de Poincare
8, play_attractor, Mostrar os pontos caminhando para os atratores
9, basin_of_attraction, Bacias de atracao
10, exit_op, Sair