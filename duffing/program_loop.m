function [flag, app ]= program_loop(operations, app)
    % Perguntar ao usu�rio qual a opera��o desejada
    op_name = menu_prompt(operations);
    % Converter o nome da opera��o para um handle para a fun��o
    fun_handle = str2func(op_name);
    % Chamar a fun��o
    [flag, app] = fun_handle(app);
end

function op_name = menu_prompt(operations)
    % This function prints the menu, prompts the user for input and returns
    % the ID of the desired operation.
    num_op = length(operations);
    
    for i = 1:num_op
        fprintf('%d. %s\n', i, operations(i).description);
    end
    opt = input('Operacao desejada: ');
    op_name = operations(opt).function_name;
end