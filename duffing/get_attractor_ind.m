function attractor_ind = get_attractor_ind(attractors_list, point, tol)
    % Confere se o ponto representado por point converge para
    % algum dos atratores em attractors_list, dentro da toler�ncia
    % tol, usando a norma 2 para comparar. Retorna a cor branca
    % caso n�o tenha convergido para nenhum dos atratores.

    num_attractors = length(attractors_list);
    attractor_ind = -1;
    
    for i = 1:num_attractors
        if norm(attractors_list(i).point - point, 2) < tol
            attractor_ind = i;
            return
        end
    end
end

