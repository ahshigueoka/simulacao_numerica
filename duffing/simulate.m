function [t, x] = simulate(sys, solver)
    sysfun = @(t, x) ss_equations(t, x, sys);
    [t, x] = solver.method(sysfun, solver.t_span, solver.x_0, solver.ode_opt);
end

