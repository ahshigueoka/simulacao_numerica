function [flag, app] = phase_plane_grid(app)
    period = app.sys.Omega*app.solver.Ts;
    app = search_attractors_aux(app, period);
    flag = true;
end

