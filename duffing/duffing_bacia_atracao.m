clear all

m = 1;
c = 0.3;
k = -1;
epsilon = 0.1;
gamma = 0;
omega = 1.2;
phi = 0;
point_attractors = {[-1 0], [0 0 1];
                    [ 1 0], [1 0 0]};

ode_opt = odeset('RelTol', 1e-4, 'Abstol', 1e-6);
t_span = [0 100];
x_0_span1 = -10:0.1:10;
x_0_span2 = -10:0.1:10;

dir_figuras = 'figuras/';

%Simular cada condi��o inicial e comparar o valor final
%com um dos pontos de equil�brio.
sys = Duffing(m, c, k, epsilon, gamma, omega, phi);
for x_i = 1:length(x_0_span1)
    for y_i = 1:length(x_0_span2)
        x_0 = [x_0_span1(x_i), x_0_span2(y_i)];
        t_span_a = sys.Omega * t_span;
        sim = SS_simulation(@sys.ss_equations, t_span, x_0, ode_opt, @ode45);

        sim.simulate;
        clear sim
    end
end