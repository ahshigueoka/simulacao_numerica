clear all

a = 1;

m = 1;
c = 0;
k = 1;
epsilon_vec = [1 1e-1 1e-2 1e-3 1e-4 -1 -1e-1 -1e-2 -1e-3 -1e-4];
gamma = 0;
omega = 0;
phi = 0;

ode_opt = odeset('RelTol', 1e-4, 'Abstol', 1e-6);
t_span = [0 100];
x_0 = [a 0];

dir_figuras = 'figures/';

num_casos = size(epsilon_vec, 2);

for it = 1:num_casos
    epsilon = epsilon_vec(it);
    sys = Duffing(m, c, k, epsilon, gamma, omega, phi);
    t_span_a = sys.Omega * t_span;
    sim = SS_simulation(@sys.ss_equations, t_span, x_0, ode_opt, @ode45);

    sim.simulate

    figure(1)
    plot(sim.x(:, 1), sim.x(:, 2));
    title('Plano de fase')
    xlabel('Posicao')
    ylabel('Velocidade')
    print('-dtiff','-r200', sprintf('%sCaso_%d__Plano_de_fase', dir_figuras, it))

    %Solucao obtida por perturbação regular
    x_pert = a*cos(sim.t) + epsilon*a^3*(-1/32*cos(sim.t) + 1/32*cos(3*sim.t) - 3/8*sim.t.*sin(sim.t));
    figure(2)
    plot(sim.t, sim.x(:, 1), sim.t, x_pert)
    title('Posicao')
    xlabel('tempo')
    ylabel('Posicao')
    print('-dtiff','-r200', sprintf('%sCaso_%d__Resposta_no_tempo_posicao', dir_figuras, it))
end