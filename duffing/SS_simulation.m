classdef SS_simulation < handle
    properties (SetAccess=private)
        t_span;
        x_0;
        ode_opt;
        method;
        t;
        x;
        ss_equations;
    end
    
    methods (Access=public)
        function obj = SS_simulation(ss_equations, t_span, x_0, ode_opt, method)
            obj.ss_equations = ss_equations;
            obj.t_span = t_span;
            obj.x_0 = x_0;
            obj.ode_opt = ode_opt;
            obj.method = method;
        end
        
        function simulate(obj)
            [obj.t, obj.x] = obj.method(@obj.ss_equations, obj.t_span, obj.x_0, obj.ode_opt);
        end
    end
end