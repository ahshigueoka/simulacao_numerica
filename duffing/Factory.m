classdef Factory < handle
    %FACTORY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods (Static, Access = public)
        %Esta fun��o instancia uma opera��o para cada tipo descrito
        %no arquivo lido
        function operations = create_operation_vector(filename)
            % Carregar as informa��es do arquivo
            options = Factory.load_menu_prompts(filename);
            %Criar o vetor com as opera��es correspondentes
            num_operations = length(options);
            
            %Inicializa��o do vetor
            op_empty = struct('ID', 'EMPTY', 'code', 0, 'description', 'empty');
            operations = {Strategy_empty(op_empty)};
            for i = 2:num_operations
                operations = {operations(:); Strategy_empty(op_empty)};
            end
            % Para cada op��o, criar uma opera��o correspondente
            for op = 1:num_operations
                operations(op) = Factory.create_operation(options(op));
            end
        end
    end
    
    % Fun��es utilit�rias que n�o dependem de instancia��o.
    methods (Static, Access = private)
        % Esta fun��o instancia os diversos tipos de opera��o
        % de acordo com o identificador fornecido.
        function op = create_operation(op)
            switch op.ID
                case 'EXIT'
                    op = Strategy_exit(op);
                otherwise
                    fprintf('Incorrect code.\n');
                    op = Strategy_empty(op);
            end 
        end
        
        % Ler o arquivo e parsear todas as entradas
        function options = load_menu_prompts(filename)
            % Abrir o documento com as entradas de menu
            fid = fopen(filename, 'r', 'b', 'UTF-8');
            
            % Ler a primeira linha
            promptline = fgetl(fid);
            options = [];
            while ischar(promptline)
                fprintf('%s\n', promptline)
                % Parsear as entradas desta linha
                fields = Factory.parse_menu_prompt(promptline);
                % Criar uma struct e armazenar no vetor de structs
                op_card = struct('ID', fields{1}, ...
                            'code', str2num(['uint8(' fields{2} ')']), ...
                            'description', fields{3});
                options = cat(1, options, op_card);
                % Continuar lendo at� terminar com o arquivo.
                promptline = fgetl(fid);
            end
            
            fclose(fid);
        end
        
        % Parsear as entradas de uma linha
        function fields = parse_menu_prompt(line)
            DELIM = ',';
            
            [token, remain] = strtok(line, DELIM);
            token = strtrim(token);
            fields = {token};
            
            while(not(isempty(remain)))
                [token, remain] = strtok(remain, DELIM);
                token = strtrim(token);
                fields = cat(1, fields, token);
            end
        end
    end
end

