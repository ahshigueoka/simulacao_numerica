classdef Strategy_empty < Strategy
    properties
        ID;
        code;
        description;
    end
    
    methods (Access = public)
        function obj = Strategy_empty(op)
            obj.ID = op.ID;
            obj.code = op.code;
            obj.description = op.description;
        end
        
        % Implementation of abstract methods
        function operation(obj, sys, app)
            fprintf('Sem efeito.\n')
        end
    end
    
end

