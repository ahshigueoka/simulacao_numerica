clear all

Fs = 1e3;
Ts = 1/Fs;
t = 0:Ts:1;
num_samples = length(t);

signal = sin(2*pi*20*t) + sin(2*pi*40*t) + 0.5*rand(size(t));

y = abs(fft(signal)/num_samples);
freq_vec = linspace(0, 1, num_samples)*Fs;

figure(1)
plot(t, signal);

figure(2)
plot(freq_vec, y);