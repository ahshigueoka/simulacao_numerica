function attractors = prompt_attractors()
    % Informar os atratores manualmente
    num_attractors = input('Informe o n�mero de atratores: ');
    % Prealocando o vetor de atratores para um ponto na origem
    % e cor branca.
    attractors(num_attractors) = struct('point', [0, 0], 'color', [1, 1, 1]);
    for i = 1:num_attractors
        fprintf('Atrator[%d]\nCoordenadas:\n', i);
        attractors(i).point(1) = input('   x: ');
        attractors(i).point(2) = input('   y: ');
        attractors(i).color(1) = input('Cor:\n   R: ');
        attractors(i).color(2) = input('   G: ');
        attractors(i).color(3) = input('   B: ');
    end

end

