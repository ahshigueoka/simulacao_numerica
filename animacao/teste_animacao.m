clear all
clc

t = (0:0.01:10)';
num_frames = length(t);
x = .1*[cos(10*t) zeros(num_frames, 1)...
        cos(10*t+2/3*pi) zeros(num_frames, 1),...
        cos(10*t+4/3*pi) zeros(num_frames, 1)];
pos = [0, 0; .25, .2; .5 0];
cores = ['r', 'g', 'b'];
frame_rate = 30;

filme = animacao(t, x, pos, cores, frame_rate);

movie(filme, 1, frame_rate);