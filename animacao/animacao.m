function Frame = animacao(t, x, pos, cores_blocos, FRAME_RATE)
    %t � o vetor com os valores de tempo
    %x � o vetor com os valores dos deslocamentos.
    % x(:, 2*i-1) ser� o deslocamento do bloco i em x, com i = 1, 2, 3
    % x(:, 2*i) ser� o deslocamento do bloco i em y, com i = 1, 2, 3
    % pos � o vetor com as posi��es m�dias dos blocos, com x na coluna 1,
    % y na coluna 2 e uma linha para cada bloco
    % cores_blocos � um vetor com um identificador de cor para cada bloco.
    % FRAME_RATE � o n�mero de frames por segundo.
    
    handle = figure;
    numFrames = length(t);

    % Taxa de atualiza��o desejada
    period = 1 / FRAME_RATE;
    movieFrames = floor(t(numFrames) / period);

    Frame(movieFrames) = struct('cdata',[],'colormap',[]);

    j = 0;
    for i = 1:numFrames
        if t(i) > j * period
            cla(handle, 'reset');
            axis([-1 2 -1 1]);
            axis square
            % Plotar o primeiro bloco
            rectangle('Position', [x(i, 1)+pos(1, 1), x(i, 2)+pos(1, 2), .05, .05], 'Facecolor', cores_blocos(1));
            set(gca,'NextPlot','add');
            % Plotar os demais blocos
            for bl = 2:size(x, 2)/2
                rectangle('Position', ...
                    [x(i, 2*bl-1)+pos(bl, 1), ...
                     x(i, 2*bl)+pos(bl, 2), .05, .05],...
                     'Facecolor', cores_blocos(bl));
            end
            j = j + 1;
            Frame(j) = getframe;
            set(gca,'NextPlot','replaceChildren');
        end
    end
end